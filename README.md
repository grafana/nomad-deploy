# Nomad Deploy: Grafana + Prometheus + Node Exporter

[![Demo](https://img.shields.io/badge/demo-dashboards-blue)](http://real-astron.nl/)

## Welcome
Welcome to the Grafana Deploy! This repository lets you set up your grafana instance in a few seconds. 

If you want to know more about monitoring, grafana or the working group who set this up, please visit: https://git.astron.nl/groups/grafana/-/wikis/home

## Prerequisites
- On your server:
  - Nomad: https://developer.hashicorp.com/nomad/docs/install
  - 

## Repository info
This repository contains the following:
- The nomad config that will spin up your grafana instance with all the persistent information
- A CI/CD deploy job so that you don't have to do manual actions

## Deploying to another environment
You can deploy the grafana instance to different environments by copy-pasting the CI/CD deploy step and changing the environment
### How you should do it:
#### Repository setup
1. create a new branch
2. create a new gitlab environment here: https://git.astron.nl/grafana/docker-deploy/-/environments/new
3. add your variables here (don't forget to set the environment!): https://git.astron.nl/grafana/docker-deploy/-/settings/ci_cd
   - `DEPLOY_HOST`: your hostname/ip address
   - `SERVICE_DIR`: the location where you want the grafana service to store the files
4. change the environment in the `gitlab-ci.yml` to your environment
5. On your server, make sure a user exists with the following configuration:
   - called `DEPLOY_USER`
   - has read, write, and execute permissions for your `SERVICE_DIR`
     - i.e. `chmod ugo+rwx <SERVICE_DIR>`
   - has in it's `authorized_keys` (i.e. `/home/gitlab-deploy/.ssh/authorized_keys`) the public key:
  ```ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJshe7NohcwEpCwL7YxG7GU/z3aCaUi1sT3M8MRhejGy gitlab-deploy@monitoring```
   - is in the `docker` group
     - i.e. ```sudo usermod -aG docker gitlab-deploy```

#### Server reverse proxy setup:
You can use any type of proxy. A few are described [here](https://grafana.com/tutorials/run-grafana-behind-a-proxy/).