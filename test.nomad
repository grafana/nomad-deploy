job "central-management" {
  datacenters = ["dc1"]
  type        = "service"

  constraint {
    attribute = "${attr.kernel.name}"
    value     = "linux"
  }

  update {
    stagger      = "30s"
    max_parallel = 1
  }

  group "grafana" {
    restart {
      attempts = 10
      interval = "5m"
      delay    = "10s"
      mode     = "delay"
    }


    network {
      mode = "bridge"
      port "http" {
        static = 3001
        to     = 3000
      }
    }

    service {
      name = "grafana"
      port = "3001"

      connect {
        sidecar_service {
          proxy {
            upstreams {
              destination_name = "prometheus"
              local_bind_port  = 9090
            }
          }
        }
      }
    }

    task "grafana" {
      driver = "docker"
      config {
        image       = "grafana/grafana:latest"
        ports       = ["http"]
        extra_hosts = [
          "prometheus:127.0.0.1"
        ]
      }

      env {
        GF_AUTH_ANONYMOUS_ENABLED     = true
        GF_AUTH_ANONYMOUS_ORG_ROLE    = "Admin"
        GF_PATH_PROVISIONING          = "/etc/grafana/provisioning"
        GF_SERVER_DOMAIN              = "[[.DEPLOY_HOST]]"
        GF_SERVER_ROOT_URL            = "%(protocol)s://%(domain)s:%(http_port)s/grafana-2/"
        GF_SERVER_SERVE_FROM_SUB_PATH = true
        GF_ALLOW_EMBEDDING            = true
      }

      resources {
        cpu    = 500
        memory = 256
      }
    }
  }


  group "prometheus" {

    network {
      mode = "bridge"

      port "prometheus" {
        to = 9090
      }
    }

    service {
      name = "prometheus"
      port = "9090"
      connect {
        sidecar_service {}
      }

      check {
        type     = "http"
        name     = "prometheus_health"
        port     = "prometheus"
        path     = "/-/healthy"
        interval = "20s"
        timeout  = "30s"
      }
    }

    task "prometheus" {
      driver = "docker"
      config {
        image = "prom/prometheus:latest"
        ports = ["prometheus"]
        mount {
          type     = "volume"
          target   = "/prometheus"
          source   = "prometheus_data"
          readonly = false
        }
      }

      resources {
        cpu    = 500
        memory = 768
      }
    }
  }
}
